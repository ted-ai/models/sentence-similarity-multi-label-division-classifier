#!/usr/bin/env bash

image_url=$1

requirements_dir=$(/busybox/sh ci/get_config_value.sh requirements_dir)
sagify_module_dir=$(/busybox/sh ci/get_config_value.sh sagify_module_dir)

echo "{\"credsStore\":\"ecr-login\"}" > /kaniko/.docker/config.json

/kaniko/executor \
  --single-snapshot \
  --context "${CI_PROJECT_DIR}" \
  --dockerfile "$sagify_module_dir/sagify_base/Dockerfile-sagemaker" \
  --destination "$image_url" \
  --build-arg module_path="$sagify_module_dir" \
  --build-arg target_dir_name="$sagify_module_dir" \
  --build-arg requirements_file_path="$requirements_dir" \
  --build-arg docker_image_base_url="${CI_DEPENDENCY_PROXY_GROUP_IMAGE_PREFIX}/python:3.8" \
  --ignore-path "//sbin"
